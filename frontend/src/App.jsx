import {
  BrowserRouter as Router,
  Routes,
  Route,
  useNavigate,
} from "react-router-dom";
import React, { useState, useEffect } from "react";
import "./App.css";
import Estate from "./components/Estate/Estate";
import axios from "axios";
import PropertyDetails from "./components/[id]";
import Login from "./components/Login";
import Register from "./components/Register";
import Profile from "./components/Profile";
import CustomerEstates from "./components/CustomerEstates";
import EditEstate from "./components/EditEstate";
import AddEstate from "./components/AddEstate";

function App() {
  const [EstatesData, setEstatesData] = useState("");
  const [customerEstatesData, setCustomerEstatesData] = useState("");

  const [loading, setLoading] = useState(true);
  const [loadingCustomer, setLoadingCustomer] = useState(true);
  const [errorCustomer, setErrorCustomer] = useState("");
  const [error, setError] = useState("");
  const [picture, setPicture] = useState([]);
  const onChangeFile = (e) => {
    setPicture(e.target.files[0]);
  };
  // user
  const user = localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : "";

  const [auth, setAuth] = useState(user ? user : "");
  const token = user ? user.token : "";
  const authId = auth ? auth.id : "";
  const authName = auth ? auth.name : "";

  // Get All Estates ///////////////////////////////////////////////////////////////////////////////

  const FetchEstates = async () => {
    setLoading(true);
    try {
      const response = await axios.get(`http://127.0.0.1:8000/api/estates`, {
        headers: { "Content-Type": "multipart/form-data" },
      });
      if (response) {
        setEstatesData(response.data.EstatesAll);
        setLoading(false);
      }
    } catch (e) {
      setError(e);
      console.error(e.message);
    }
  };

  // Get Customer Estates ///////////////////////////////////////////////////////////////////////////////
  const FetchCustomerEstates = async () => {
    setLoadingCustomer(true);
    try {
      const response = await axios.get(
        `http://127.0.0.1:8000/api/customer/estates/${authId}`,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      if (response) {
        setCustomerEstatesData(response.data.customerEstates);
        setLoadingCustomer(false);
      }
    } catch (e) {
      setErrorCustomer(e);
      console.error(e.message);
    }
  };

  // if (loading || loadingCat || loading1) return <LoadingEffect />;
  // if (loading) return "loading...";
  // if (error) return "Error";
  return (
    <Router>
      <>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route
            path="/Profile"
            element={<Profile auth={auth} setAuth={setAuth} />}
          />
          <Route
            path="/register"
            element={<Register auth={auth} setAuth={setAuth} />}
          />
          <Route
            path="/"
            element={
              <Estate
                FetchEstates={FetchEstates}
                EstatesData={EstatesData}
                loading={loading}
              />
            }
          />
          <Route
            path="/MyEstates"
            element={
              <CustomerEstates
                FetchCustomerEstates={FetchCustomerEstates}
                customerEstatesData={customerEstatesData}
                loadingCustomer={loadingCustomer}
                authName={authName}
                authId={authId}
              />
            }
          />
          <Route path="/property/:id" element={<PropertyDetails />} />
          <Route path="/editEstate/:id" element={<EditEstate />} />
          <Route path="/AddEstate/:id" element={<AddEstate />} />
        </Routes>
      </>
    </Router>
  );
}

export default App;
