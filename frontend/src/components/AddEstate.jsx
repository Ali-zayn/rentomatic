import React from "react";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import "./EditEstate.css";
import Navbar from "./Navbar";
import { useParams } from "react-router-dom";
import Popup from "./Popup";

import { FcCameraAddon } from "react-icons/fc";

function AddEstate() {
  const { id } = useParams();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadingCity, setLoadingCity] = useState(true);
  const [loadingType, setLoadingType] = useState(true);
  const [loadingPeriod, setLoadingPeriod] = useState(true);
  const [LoadingAddPicyures, setLoadingAddPicyures] = useState(false);
  const [cityData, setCityData] = useState(null);
  const [estateTypeData, setEstateTypeData] = useState(null);
  const [rentPeriodData, setRentPeriodData] = useState(null);
  const [addressAdd, setAddressAdd] = useState(null);
  const [popup2, setPopup2] = useState(false);
  const [imagesAdd, setImagesAdd] = useState(null);

  const [idApp, setIdApp] = useState(null);
  const [areaAdd, setAreaAdd] = useState(null);
  const [bathroomsAdd, setBathroomsAdd] = useState(null);
  const [roomsAdd, setRoomsAdd] = useState(null);
  const [contactNumberAdd, setContactNumberAdd] = useState(null);
  const [coverImageAdd, setCoverImageAdd] = useState(null);
  const [descriptionAdd, setDescriptionAdd] = useState(null);
  const [isAvailableAdd, setIsAvailableAdd] = useState(null);
  const [isFurnishedAdd, setIsFurnishedAdd] = useState(null);
  const [rentFrequencyAdd, setRentFrequencyAdd] = useState(null);
  const [cityAdd, setCityAdd] = useState(null);
  const [rentPeriodAdd, setRentPeriodAdd] = useState(null);
  const [estateTypeAdd, setEstateTypeAdd] = useState(null);

  const onChangeFile = (e) => {
    setCoverImageAdd(e.target.files[0]);
  };
  // Get All cities ///////////////////////////////////////////////////////////////////////////////
  const FetchCityData = async () => {
    setLoadingCity(true);
    await axios
      .get(`http://127.0.0.1:8000/api/cities`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setCityData(res.data);

        setLoadingCity(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  // Get All Estate Types //////;/////////////////////////////////////////////////////////////////////////
  const FetchTypeData = async () => {
    setLoadingType(true);
    await axios
      .get(`http://127.0.0.1:8000/api/estateTypes`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setEstateTypeData(res.data);
        setLoadingType(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  // Get All Estate Rent Periods ///////////////////////////////////////////////////////////////////////////////
  const FetchPeriodsData = async () => {
    setLoadingPeriod(true);
    await axios
      .get(`http://127.0.0.1:8000/api/rentPeriods`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setRentPeriodData(res.data);
        setLoadingPeriod(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  const AddEstate = () => {
    setLoading(true);

    const formData = new FormData();
    formData.append("customer_id", id);
    formData.append("address", addressAdd);
    formData.append("area", areaAdd);
    formData.append("bathrooms", bathroomsAdd);
    formData.append("city_id", cityAdd);
    formData.append("contact_number", contactNumberAdd);

    formData.append("cover_image", coverImageAdd);

    formData.append("estate_type_id", estateTypeAdd);
    formData.append("description", descriptionAdd);
    formData.append("isAvailable", isAvailableAdd);
    formData.append("isFurnished", isFurnishedAdd);
    formData.append("rent_frequency", rentFrequencyAdd);
    formData.append("rooms", roomsAdd);
    formData.append("rent_period_id", rentPeriodAdd);
    axios
      .post(`http://127.0.0.1:8000/api/addEstate`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setIdApp(res.data.id);
        console.log("sent");
        setLoading(false);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };
  console.log(idApp);
  const AddPictures = () => {
    setLoadingAddPicyures(true);
    const formData = new FormData();
    formData.append("estate_id", idApp);
    formData.append("image_url", coverImageAdd);
    axios
      .post(`http://127.0.0.1:8000/api/addImages`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        console.log("sent");
        setLoadingAddPicyures(false);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  useEffect(() => {
    FetchCityData();
    FetchTypeData();
    FetchPeriodsData();
  }, []);

  if (
    loading ||
    loadingCity ||
    loadingPeriod ||
    loadingType ||
    LoadingAddPicyures
  )
    return "...loading";

  return (
    <>
      <Navbar />
      <form
        onSubmit={() => {
          AddEstate();
          setPopup2(true);
        }}
      >
        <div className="Edit-box">
          <h2>Add Estate</h2>
          <div className="Edit-flex-box">
            <div className="user-box">
              <label>Address:</label>
              <input
                name="Address"
                type="text"
                value={addressAdd}
                placeholder="Address.."
                onChange={(e) => {
                  setAddressAdd(e.target.value);
                }}
                required
              />
              <label> Rooms:</label>
              <input
                name="name"
                type="text"
                value={roomsAdd}
                placeholder="Rooms.."
                onChange={(e) => {
                  setRoomsAdd(e.target.value);
                }}
                required
              />

              <label>Area:</label>
              <input
                value={areaAdd}
                name="Area"
                type="text"
                placeholder="Area"
                onChange={(e) => {
                  setAreaAdd(e.target.value);
                }}
                required
              />
              <label>Bathrooms:</label>
              <input
                value={bathroomsAdd}
                name="Bathrooms"
                type="text"
                placeholder="Bathrooms..."
                onChange={(e) => {
                  setBathroomsAdd(e.target.value);
                }}
                required
              />
            </div>
          </div>
          <div className="Edit-flex-box">
            <div className="user-box">
              <label>Contact Number:</label>
              <input
                value={contactNumberAdd}
                name="Area"
                type="text"
                placeholder="Contact Number..."
                onChange={(e) => {
                  setContactNumberAdd(e.target.value);
                }}
                required
              />

              <label>Rent Amount:</label>
              <input
                value={rentFrequencyAdd}
                name="Bathrooms"
                type="text"
                placeholder="Rent Amount..."
                onChange={(e) => {
                  setRentFrequencyAdd(e.target.value);
                }}
                required
              />
              <label>Furnished:</label>
              <input
                value={isFurnishedAdd}
                name="Area"
                type="text"
                placeholder="Furnished..."
                onChange={(e) => {
                  setIsFurnishedAdd(e.target.value);
                }}
                required
              />

              <label>Available:</label>
              <input
                value={isAvailableAdd}
                name="Bathrooms"
                type="text"
                placeholder="Available..."
                onChange={(e) => {
                  setIsAvailableAdd(e.target.value);
                }}
                required
              />
            </div>
          </div>

          <div className="Edit-flex-box">
            <div className="user-box">
              <label htmlFor="title">Rent Period:</label>
              <select
                className="currency-list"
                value={rentPeriodAdd}
                onChange={(e) => {
                  setRentPeriodAdd(e.target.value);
                }}
                required
              >
                {rentPeriodData.map((item) => (
                  <option value={item.id}>{item.rent_period}</option>
                ))}
              </select>
              <label htmlFor="title">city:</label>
              <select
                className="currency-list"
                value={cityAdd}
                onChange={(e) => {
                  setCityAdd(e.target.value);
                }}
                required
              >
                {cityData.map((item) => (
                  <option value={item.id}>{item.city_name}</option>
                ))}
              </select>

              <label htmlFor="title">Estate Type:</label>
              <select
                className="currency-list"
                value={estateTypeAdd}
                onChange={(e) => {
                  setEstateTypeAdd(e.target.value);
                }}
                required
              >
                {estateTypeData.map((item) => (
                  <option value={item.id}>{item.estate_type}</option>
                ))}
              </select>
            </div>
          </div>
          <div className="Edit-flex-box">
            <div className="description">
              <label>Description:</label>
              <input
                width="500px"
                height="200px"
                style={{
                  inlineSize: "150px",
                  overflowWrap: "break-word",
                  wordBreak: "break-all",
                }}
                value={descriptionAdd}
                name="Aryea"
                type="text"
                placeholder="Description..."
                onChange={(e) => {
                  setDescriptionAdd(e.target.value);
                }}
                required
              />
            </div>
          </div>
          <input type="file" name="picture" onChange={onChangeFile} required />
          <div className="button-form">
            <input value="confirm" id="submit" type="submit" />
          </div>
        </div>
      </form>
      <Popup trigger={popup2} setTrigger={setPopup2}>
        <div className="popup_title">
          <h4>
            <FcCameraAddon />
            Add Pictures
          </h4>
        </div>
        <form
          onSubmit={() => {
            AddPictures();
          }}
        >
          <input type="file" name="picture" onChange={onChangeFile} required />
          <div className="popup_buttons">
            <input type="submit" className="confirm_btn" value="Confirm" />
            <button
              className="confirm_btn"
              value="done"
              onClick={() => {
                setPopup2(false);
              }}
            >
              Done
            </button>
          </div>
        </form>
      </Popup>
    </>
  );
}

export default AddEstate;
