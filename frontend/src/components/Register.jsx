import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Navbar from "./Navbar";

const Register = () => {
  const navigate = useNavigate();
  const [addname, setAddname] = useState("");
  const [addemail, setAddemail] = useState("");
  const [passwordAdd, setPasswordAdd] = useState("");
  const [confAdd, setConfAdd] = useState("");
  const AddAdmin = () => {
    if (addname.length >= 6) {
      if (passwordAdd.length >= 6) {
        if (passwordAdd === confAdd) {
          const formData = new FormData();
          formData.append("name", addname);
          formData.append("password", passwordAdd);
          formData.append("email", addemail);
          alert("user Added");
          navigate("/login");
          axios
            .post(`http://127.0.0.1:8000/api/addCustomer`, formData, {
              headers: { "Content-Type": "multipart/form-data" },
            })
            .then((res) => {
              console.log("sent");
            })
            .catch((error) => {
              console.log(error.message);
            });
        } else {
          return alert("Please confirm the password correctly");
        }
      } else {
        alert("The password must be at least 6 characters long.");
      }
    } else {
      alert("The name must be at least 6 characters long.");
    }
  };

  return (
    <>
      <div>
        <Navbar />
      </div>
      <div className="login-background">
        <div className="login-box">
          <h2>Register</h2>
          <form
            onSubmit={() => {
              AddAdmin();
            }}
          >
            <div className="user-box">
              <input
                type="text"
                name="name"
                required
                onChange={(e) => {
                  setAddname(e.target.value);
                }}
              />
              <label>Name</label>
            </div>
            <div className="user-box">
              <input
                type="email"
                name="email"
                required
                onChange={(e) => {
                  setAddemail(e.target.value);
                }}
              />
              <label>Email</label>
            </div>
            <div className="user-box">
              <input
                type="password"
                name="login"
                required
                onChange={(e) => {
                  setPasswordAdd(e.target.value);
                }}
              />
              <label>Password</label>
            </div>
            <div className="user-box">
              <input
                type="password"
                name="login"
                required
                onChange={(e) => {
                  setConfAdd(e.target.value);
                }}
              />
              <label>Confirm Password</label>
            </div>
            <div className="button-form">
              <input type="submit" id="submit" value="Submit" />
              <div id="register">
                Already have an account?
                <Link to="/login">
                  <p>Log In</p>
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Register;
