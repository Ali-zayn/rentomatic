import { Flex, Box, Text } from "@chakra-ui/react";
import MyEstates from "./MyEstates";
import { useEffect } from "react";
import Navbar from "./Navbar";
import { MdAddBusiness } from "react-icons/md";
import { useNavigate } from "react-router-dom";

export const Banner = ({ purpose, title1, title2, desc1, desc2, imageUrl }) => (
  <Flex flexWrap="wrap" justifyContent="center" alignItems="center">
    {/*<Image src={imageUrl} width={500} height={300} />*/}
    <Box>
      <Text fontSize="large" fontWeight="bold">
        {title1}
      </Text>

      <Text
        fontSize="xxx-large"
        fontWeight="bold"
        color="lightblue"
        className="MyEstate"
      >
        {title2}
      </Text>
      <Text fontSize="large" color="gray">
        {desc1}
      </Text>
    </Box>
  </Flex>
);

const CustomerEstates = ({
  FetchCustomerEstates,
  loadingCustomer,
  customerEstatesData,
  authName,
  authId,
}) => {
  useEffect(() => {
    FetchCustomerEstates();
  }, []);
  let navigate = useNavigate();
  return (
    <>
      <Navbar />

      <Box>
        <div className="MyEstateBody">
          <div className="messageBody">
            <span></span>
            <div className="message">
              <div className="word1">Welcome</div>
              <div className="word2">To Your</div>
              <div className="word3">Home</div>
            </div>
          </div>
        </div>
        <div className="MyEstateBody">
          <div className="MyEstate">
            <h2>{authName}</h2>
            <h2>{authName}</h2>
          </div>
        </div>
        <Banner desc1=" Here are your Appartments" />
        <Flex
          flexWrap="wrap"
          justifyContent="center"
          alignItems="center"
          m="10"
        >
          <MdAddBusiness
            fontSize="xxx-large"
            className="profile_web_edit"
            color="lightblue"
            width="30px"
            style={{ borderStyle: "solid" }}
            onClick={() => {
              navigate(`/AddEstate/${authId}`);
            }}
          />
        </Flex>
        <Flex flexWrap="wrap">
          {!loadingCustomer
            ? customerEstatesData.map((customerEstatesData) => (
                <MyEstates
                  FetchCustomerEstates={FetchCustomerEstates}
                  customerEstatesData={customerEstatesData}
                  key={customerEstatesData.id}
                />
              ))
            : "Loading..."}
        </Flex>
      </Box>
    </>
  );
};

export default CustomerEstates;
