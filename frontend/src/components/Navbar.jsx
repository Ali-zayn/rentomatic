import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  IconButton,
  Flex,
  Box,
  Spacer,
} from "@chakra-ui/react";
import {
  FcSearch,
  FcHome,
  FcMenu,
  FcPortraitMode,
  FcUnlock,
  FcDecision,
  FcDepartment,
  FcExport,
} from "react-icons/fc";

import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import "./Navbar.css";

function Navbar() {
  const user = localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : "";

  const [auth, setAuth] = useState(user ? user : null);
  function deleteCustomer() {
    localStorage.clear();
  }
  let navigate = useNavigate();
  function routeChange() {
    let path = `/`;
    navigate(path);
  }
  return (
    <Flex
      p="2"
      border="0.4rem"
      borderStyle="solid"
      borderColor="gray"
      background="lightblue"
    >
      <Box
        fontSize="xxx-large"
        color="blue"
        fontWeight="bold"
        className="animate-charcter"
      >
        <Link to={`/`} paddingleft="2" Link style={{ textDecoration: "none" }}>
          Rentomatic
        </Link>
      </Box>
      <Spacer />
      <Box>
        <Menu>
          <MenuButton
            listStyleType="none"
            as={IconButton}
            icon={<FcMenu />}
            variant="outline"
            width={50}
            height={50}
            color="red"
            fontSize="x-large"
          />
          <MenuList zIndex="1">
            <Link to={`/`} Link style={{ textDecoration: "none" }}>
              <MenuItem
                icon={<FcHome />}
                width={160}
                height={40}
                fontSize="x-large"
              >
                Home
              </MenuItem>
            </Link>
            {auth ? (
              <Link to={`/Profile`} Link style={{ textDecoration: "none" }}>
                <MenuItem
                  icon={<FcPortraitMode />}
                  width={160}
                  height={40}
                  fontSize="x-large"
                >
                  Profile
                </MenuItem>
              </Link>
            ) : (
              console.log("not a customer")
            )}
            {!auth ? (
              <Link to={`/login`} Link style={{ textDecoration: "none" }}>
                <MenuItem
                  icon={<FcUnlock />}
                  width={160}
                  height={40}
                  fontSize="x-large"
                >
                  Log-in
                </MenuItem>
              </Link>
            ) : (
              console.log("user")
            )}
            {!auth ? (
              <Link to={`/register`} Link style={{ textDecoration: "none" }}>
                <MenuItem
                  icon={<FcDecision />}
                  width={160}
                  height={40}
                  fontSize="x-large"
                >
                  Register
                </MenuItem>
              </Link>
            ) : (
              console.log("user")
            )}
            {auth ? (
              <Link to={`/MyEstates`} Link style={{ textDecoration: "none" }}>
                <MenuItem
                  icon={<FcDepartment />}
                  width={160}
                  height={40}
                  fontSize="x-large"
                >
                  MyEstates
                </MenuItem>
              </Link>
            ) : (
              console.log("not a customer")
            )}
            {auth ? (
              <MenuItem
                icon={<FcExport />}
                width={160}
                height={40}
                color="blue"
                fontSize="x-large"
                onClick={() => {
                  deleteCustomer();
                  routeChange();
                }}
              >
                Log-out
              </MenuItem>
            ) : (
              console.log("not a customer")
            )}
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  );
}

export default Navbar;
