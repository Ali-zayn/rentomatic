import React from "react";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import "./EditEstate.css";
import Navbar from "./Navbar";
import { useParams } from "react-router-dom";

function EditEstate() {
  const { id } = useParams();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadingEstate, setLoadingEstate] = useState(true);
  const [loadingCity, setLoadingCity] = useState(true);
  const [loadingType, setLoadingType] = useState(true);
  const [loadingPeriod, setLoadingPeriod] = useState(true);
  const [websites, setWebsites] = useState(false);
  const [cityData, setCityData] = useState(null);
  const [estateTypeData, setEstateTypeData] = useState(null);
  const [rentPeriodData, setRentPeriodData] = useState(null);

  const [addressEdit, setAddressEdit] = useState(null);
  const [areaEdit, setAreaEdit] = useState(null);
  const [bathroomsEdit, setBathroomsEdit] = useState(null);
  const [roomsEdit, setRoomsEdit] = useState("");
  const [contactNumberEdit, setContactNumberEdit] = useState(null);
  const [coverImageEdit, setCoverImageEdit] = useState(null);
  const [descriptionEdit, setDescriptionEdit] = useState(null);
  const [isAvailableEdit, setIsAvailableEdit] = useState(null);
  const [isFurnishedEdit, setIsFurnishedEdit] = useState(null);
  const [rentFrequency, setRentFrequency] = useState(null);
  const [cityEdit, setCityEdit] = useState(null);
  const [rentPeriodEdit, setRentPeriodEdit] = useState(null);
  const [estateTypeEdit, setEstateTypeEdit] = useState(null);

  const onChangeFile = (e) => {
    setCoverImageEdit(e.target.files[0]);
  };

  const getEstate = async () => {
    setLoadingEstate(true);
    try {
      const response = await axios.get(
        `http://127.0.0.1:8000/api/estate/${id}`,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      if (response) {
        setAddressEdit(response.data[0].address);
        setAreaEdit(response.data[0].area);
        setBathroomsEdit(response.data[0].bathrooms);
        setCityEdit(response.data[0].city_id);
        setRoomsEdit(response.data[0].rooms);
        setContactNumberEdit(response.data[0].contact_number);
        setCoverImageEdit(response.data[0].cover_image);
        setIsAvailableEdit(response.data[0].isAvailable);
        setIsFurnishedEdit(response.data[0].isFurnished);
        setRentFrequency(response.data[0].rent_frequency);
        setRentPeriodEdit(response.data[0].rent_period_id);
        setEstateTypeEdit(response.data[0].estate_type_id);
        setDescriptionEdit(response.data[0].description);
        setLoadingEstate(false);
      }
    } catch (e) {
      setError(e);
      console.error(e.message);
    }
  };

  useEffect(() => {
    getEstate();
    FetchCityData();
    FetchTypeData();
    FetchPeriodsData();
  }, []);

  // Get All cities ///////////////////////////////////////////////////////////////////////////////
  const FetchCityData = async () => {
    setLoadingCity(true);
    await axios
      .get(`http://127.0.0.1:8000/api/cities`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setCityData(res.data);

        setLoadingCity(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  // Get All Estate Types //////;/////////////////////////////////////////////////////////////////////////
  const FetchTypeData = async () => {
    setLoadingType(true);
    await axios
      .get(`http://127.0.0.1:8000/api/estateTypes`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setEstateTypeData(res.data);
        setLoadingType(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  // Get All Estate Rent Periods ///////////////////////////////////////////////////////////////////////////////
  const FetchPeriodsData = async () => {
    setLoadingPeriod(true);
    await axios
      .get(`http://127.0.0.1:8000/api/rentPeriods`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setRentPeriodData(res.data);
        setLoadingPeriod(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  console.log(typeof coverImageEdit);

  const EditEstate = async () => {
    setLoading(true);
    await axios
      .put(
        `http://127.0.0.1:8000/api/editEstates/${id}?address=${addressEdit}&area=${areaEdit}&bathrooms=${bathroomsEdit}&estate_type_id=${estateTypeEdit}&city_id=${cityEdit}&contact_number=${contactNumberEdit}&description=${descriptionEdit}&isAvailable=${isAvailableEdit}&isFurnished=${isFurnishedEdit}&rent_frequency=${rentFrequency}&rooms=${roomsEdit}&rent_period_id=${rentPeriodEdit}`,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      )
      .then((res) => {
        setLoading(false);
        setWebsites(!websites);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  // const EditEstate = async () => {
  //   setLoading(true);
  //   const formData = new FormData();
  //   formData.append("address", addressEdit);
  //   formData.append("area", areaEdit);
  //   formData.append("bathrooms", bathroomsEdit);
  //   formData.append("city_id", cityEdit);
  //   formData.append("contact_number", contactNumberEdit);
  //
  //   formData.append("cover_image", coverImageEdit);
  //
  //   formData.append("estate_type_id", estateTypeEdit);
  //   formData.append("description", descriptionEdit);
  //   formData.append("isAvailable", isAvailableEdit);
  //   formData.append("isFurnished", isFurnishedEdit);
  //   formData.append("rent_frequency", rentFrequency);
  //   formData.append("rooms", roomsEdit);
  //   formData.append("rent_period_id", rentPeriodEdit);
  //   await axios
  //     .put(`http://127.0.0.1:8000/api/editEstates/${id}`, formData, {
  //       headers: { "Content-Type": "multipart/form-data" },
  //     })
  //     .then((res) => {
  //       console.log("updated");
  //       setLoading(false);
  //     })
  //
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };
  if (loading || loadingCity || loadingEstate || loadingPeriod || loadingType)
    return "...loading";

  return (
    <>
      <Navbar />

      <form
        onSubmit={() => {
          EditEstate();
        }}
      >
        <div className="Edit-box">
          <h2>Update Estate</h2>
          <div className="Edit-flex-box">
            <div className="user-box">
              <label>Address:</label>
              <input
                name="Address"
                type="text"
                value={addressEdit}
                placeholder="Address.."
                onChange={(e) => {
                  setAddressEdit(e.target.value);
                }}
              />
              <label> rooms:</label>
              <input
                name="name"
                type="text"
                value={roomsEdit}
                placeholder="Name.."
                onChange={(e) => {
                  setRoomsEdit(e.target.value);
                }}
              />
              <label>Area:</label>
              <input
                value={areaEdit}
                name="Area"
                type="text"
                placeholder="Area"
                onChange={(e) => {
                  setAreaEdit(e.target.value);
                }}
              />
              <label>Bathrooms:</label>
              <input
                value={bathroomsEdit}
                name="Bathrooms"
                type="text"
                placeholder="Bathrooms..."
                onChange={(e) => {
                  setBathroomsEdit(e.target.value);
                }}
              />
            </div>
          </div>
          <div className="Edit-flex-box">
            <div className="user-box">
              <label>Contact Number:</label>
              <input
                value={contactNumberEdit}
                name="Area"
                type="text"
                placeholder="Area"
                onChange={(e) => {
                  setContactNumberEdit(e.target.value);
                }}
              />

              <label>Rent Amount:</label>
              <input
                value={rentFrequency}
                name="Bathrooms"
                type="text"
                placeholder="Bathrooms..."
                onChange={(e) => {
                  setRentFrequency(e.target.value);
                }}
              />
              <label>furnished:</label>
              <input
                value={isFurnishedEdit}
                name="Area"
                type="text"
                placeholder="Area"
                onChange={(e) => {
                  setIsFurnishedEdit(e.target.value);
                }}
              />

              <label>Available:</label>
              <input
                value={isAvailableEdit}
                name="Bathrooms"
                type="text"
                placeholder="Bathrooms..."
                onChange={(e) => {
                  setIsAvailableEdit(e.target.value);
                }}
              />
            </div>
          </div>

          <div className="Edit-flex-box">
            <div className="user-box">
              <label htmlFor="title">Rent Period:</label>
              <select
                className="currency-list"
                value={rentPeriodEdit}
                onChange={(e) => {
                  setRentPeriodEdit(e.target.value);
                }}
              >
                {rentPeriodData.map((item) => (
                  <option value={item.id}>{item.rent_period}</option>
                ))}
              </select>
              <label htmlFor="title">city:</label>
              <select
                className="currency-list"
                value={cityEdit}
                onChange={(e) => {
                  setCityEdit(e.target.value);
                }}
              >
                {cityData.map((item) => (
                  <option value={item.id}>{item.city_name}</option>
                ))}
              </select>

              <label htmlFor="title">Estate Type:</label>
              <select
                className="currency-list"
                value={estateTypeEdit}
                onChange={(e) => {
                  setEstateTypeEdit(e.target.value);
                }}
              >
                {estateTypeData.map((item) => (
                  <option value={item.id}>{item.estate_type}</option>
                ))}
              </select>
            </div>
          </div>
          <div className="Edit-flex-box">
            <div>
              <label>Description:</label>
              <input
                width="500px"
                height="200px"
                style={{
                  inlineSize: "150px",
                  overflowWrap: "break-word",
                  wordBreak: "break-all",
                }}
                value={descriptionEdit}
                name="Aryea"
                type="text"
                placeholder="Area"
                onChange={(e) => {
                  setDescriptionEdit(e.target.value);
                }}
              />
            </div>
          </div>
          <input type="file" name="picture" onChange={onChangeFile} />
          <div className="button-form">
            <input value="confirm" id="submit" type="submit" />
          </div>
        </div>
      </form>
    </>
  );
}

export default EditEstate;
