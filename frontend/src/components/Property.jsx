import { Box, Flex, Text } from "@chakra-ui/layout";
import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { GoVerified } from "react-icons/go";
import millify from "millify";
import { Link } from "react-router-dom";
import { CgUnavailable } from "react-icons/cg";
import "./Property.css";
// cover_image,
//     contact_number,
//     rent_frequency,
//     rent_period_id,
//     rooms,
//     city_name,
//     address,
//     bathrooms,
//     area,
//     isAvailable,
//     description,
//     id,

const Property = ({ EstatesData }) => (
  <Link
    to={`/property/${EstatesData.id}`}
    Link
    style={{ textDecoration: "none" }}
  >
    <div className="properties-container">
      <div className="box">
        <Box>
          <Box>
            <img
              src={`http://localhost:8000/pictures/${EstatesData.cover_image}`}
              width={330}
              height={260}
              alt="Logo"
            />

            {/*<Image src={cover_image ? cover_image.url : <p>image</p>} width={400} height={260} />*/}
          </Box>
          <Flex
            alignItems="center"
            p="1"
            justifyContent="space-between"
            color="blue"
          >
            Location : {EstatesData.city.city_name} ,{EstatesData.address}{" "}
            {EstatesData.isFurnished ? (
              <Box color="green" w="6rem">
                Furnished <GoVerified />
              </Box>
            ) : (
              <Box color="red" w="7.5rem">
                Unfurnished <CgUnavailable />
              </Box>
            )}
          </Flex>
          <Box w="full">
            <Flex
              alignItems="center"
              p="1"
              justifyContent="space-between"
              color="black"
            >
              <Text fontWeight="bold" fontSize="lg">
                {EstatesData.rent_frequency}${EstatesData.period.rent_period}
              </Text>
              <Text fontWeight="bold" fontSize="lg">
                {" "}
                Type : {EstatesData.type.estate_type}
              </Text>
            </Flex>
            <Flex
              alignItems="center"
              p="1"
              justifyContent="center"
              color="black"
            >
              <Text fontWeight="bold" fontSize="lg">
                Contact Number: {EstatesData.contact_number}
              </Text>
            </Flex>

            <Flex
              alignItems="center"
              p="1"
              justifyContent="center"
              w="auto"
              color="blue"
            >
              <Box display="flex" justifyContent="space-between" w="250px">
                {EstatesData.rooms}
                <FaBed />| {EstatesData.bathrooms} <FaBath /> |{" "}
                {millify(EstatesData.area)} sqft <BsGridFill />
              </Box>
            </Flex>
            <Text fontSize="lg" color="darkslategray">
              {EstatesData.description.length > 50
                ? EstatesData.description.substring(0, 50) + "..."
                : EstatesData.description}
            </Text>
          </Box>
        </Box>
      </div>
    </div>
  </Link>
);

export default Property;
