import React, { useState, useEffect } from "react";
import "./Login.css";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import Navbar from "./Navbar";

function Login() {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [data, setData] = useState("");

  const [error, setError] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    fetch(
      `http://127.0.0.1:8000/api/login?email=${email}&password=${password}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
        //
        // if(data.length === 0){
        //     return alert("incorrect username and password");
        // }
        if (data.status.length === 0) {
          return alert("incorrect username and password");
        } else {
          localStorage.setItem("user", JSON.stringify(data.status[0]));
          localStorage.setItem("token", data.token);
          localStorage.setItem("id", data.status[0].id);
          navigate("/");
        }
      });
  };

  if (error) return "Error...";

  return (
    <>
      <Navbar />
      <div className="login-background">
        <div className="login-box">
          <h2>Login</h2>

          <form onSubmit={onSubmit}>
            <div className="user-box">
              <input
                type="text"
                name="login"
                required
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              <label>Email</label>
            </div>
            <div className="user-box">
              <input
                type="password"
                name="login"
                required
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
              <label>Password</label>
            </div>
            <div className="button-form">
              <input type="submit" id="submit" value="Submit" />
              <div id="register">
                Don't have an account?
                <Link to="/register">
                  <p>Register</p>
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default Login;
