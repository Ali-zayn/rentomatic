import React from "react";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import Navbar from "./Navbar";

const Profile = ({ auth: { id, first_name, last_name, email } }) => {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [websites, setWebsites] = useState(false);

  const [nameEdit, setNameEdit] = useState("");
  const [emailEdit, setEmailEdit] = useState("");
  const [UserPassword, setUserPassword] = useState("");
  console.log(id);

  const getUser = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `http://127.0.0.1:8000/api/customer/${id}`,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      if (response) {
        setUserPassword(response.data.customer[0].password);
        setNameEdit(response.data.customer[0].name);
        setEmailEdit(response.data.customer[0].email);
        setLoading(false);
      }
    } catch (e) {
      setError(e);
      console.error(e.message);
    }
  };

  console.log(UserPassword);
  useEffect(() => {
    getUser();
  }, []);

  const EditProfile = async () => {
    setLoading(true);
    await axios
      .put(
        `http://127.0.0.1:8000/api/editCustomer/${id}?name=${nameEdit}&email=${emailEdit}&password=${UserPassword}`
      )
      .then((res) => {
        setLoading(false);
        setWebsites(!websites);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };
  if (loading) return "...loading";

  return (
    <>
      <Navbar />
      <div className="login-background">
        <div className="login-box">
          <form
            onSubmit={() => {
              EditProfile();
            }}
          >
            <h2>Update Profile</h2>
            <div className="user-box">
              <input
                name="name"
                type="text"
                value={nameEdit}
                placeholder="Name.."
                onChange={(e) => {
                  setNameEdit(e.target.value);
                }}
              />
              <label> Name:</label>
            </div>
            <div className="user-box">
              <input
                value={emailEdit}
                name="email"
                type="email"
                placeholder="E-Mail"
                onChange={(e) => {
                  setEmailEdit(e.target.value);
                }}
              />
              <label>Email address:</label>
            </div>
            <div className="user-box">
              <input
                value={UserPassword}
                name="pass"
                type="password"
                placeholder="password"
                onChange={(e) => {
                  setUserPassword(e.target.value);
                }}
              />
              <label>Password:</label>
            </div>
            <div className="button-form">
              <input value="Submit" id="submit" type="submit" />
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Profile;
