import { Box, Flex, Text } from "@chakra-ui/layout";
import axios from "axios";
import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { GoVerified } from "react-icons/go";
import millify from "millify";
import Popup from "./Popup";
import { MdOutlineDelete } from "react-icons/md";
import { CgUnavailable } from "react-icons/cg";
import { BiEdit } from "react-icons/bi";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// cover_image,
//     contact_number,
//     rent_frequency,
//     rent_period_id,
//     rooms,
//     city_name,
//     address,
//     bathrooms,
//     area,
//     isAvailable,
//     description,
//     id,

function MyEstates({ FetchCustomerEstates, customerEstatesData }) {
  const [loadingDelete, setLoadingDelete] = useState(true);
  const [errorDelete, setErrorDelete] = useState("");
  const [transID, setTransID] = useState("");
  const [websites, setWebsites] = useState(false);
  const [popupCur, setPopupCur] = useState(false);
  let navigate = useNavigate();
  const routeChange = () => {
    let path = `/editEstate`;
    navigate(path);
  };

  const DeleteWebsite = async () => {
    setLoadingDelete(true);
    await axios
      .delete(`http://127.0.0.1:8000/api/deleteEstates/${transID}`)
      .then((res) => {
        setWebsites(!websites);
        setLoadingDelete(false);
        window.location.reload();
      })
      .catch((error) => {
        setErrorDelete(error);
        console.error(error.message);
      });
  };
  // if (loading || loadingCat || loading1) return <LoadingEffect />
  if (errorDelete) return "Error";
  return (
    <div className="properties-container">
      <div className="box">
        <Box>
          <Box>
            <img
              src={`http://localhost:8000/pictures/${customerEstatesData.cover_image}`}
              width={330}
              height={260}
              alt="Logo"
            />

            {/*<Image src={cover_image ? cover_image.url : <p>image</p>} width={400} height={260} />*/}
          </Box>
          <Flex
            alignItems="center"
            p="1"
            justifyContent="space-between"
            color="blue"
          >
            Location : {customerEstatesData.city.city_name} ,
            {customerEstatesData.address}{" "}
            {customerEstatesData.isAvailable ? (
              <Box color="green">
                Furnished <GoVerified />
              </Box>
            ) : (
              <Box color="red">
                Unfurnished <CgUnavailable />
              </Box>
            )}
          </Flex>
          <Box w="full">
            <Flex
              alignItems="center"
              p="1"
              justifyContent="space-between"
              color="black"
            >
              <Text fontWeight="bold" fontSize="lg">
                {customerEstatesData.rent_frequency}$
                {customerEstatesData.period.rent_period}
              </Text>
              <Text fontWeight="bold" fontSize="lg">
                {" "}
                Type : {customerEstatesData.type.estate_type}
              </Text>
            </Flex>
            <Flex
              alignItems="center"
              p="1"
              justifyContent="center"
              color="black"
            >
              <Text fontWeight="bold" fontSize="lg">
                Contact Number: {customerEstatesData.contact_number}
              </Text>
            </Flex>

            <Flex
              alignItems="center"
              p="1"
              justifyContent="center"
              w="auto"
              color="blue"
            >
              <Box display="flex" justifyContent="space-between" w="250px">
                {customerEstatesData.rooms}
                <FaBed />| {customerEstatesData.bathrooms} <FaBath /> |{" "}
                {millify(customerEstatesData.area)} sqft <BsGridFill />
              </Box>
            </Flex>
            <Text fontSize="lg" color="darkslategray">
              {customerEstatesData.description.length > 50
                ? customerEstatesData.description.substring(0, 50) + "..."
                : customerEstatesData.description}
            </Text>
            <Flex p="1" justifyContent="space-evenly" color="red">
              <MdOutlineDelete
                fontSize="xx-large"
                className="profile_web_edit"
                onClick={() => {
                  setTransID(customerEstatesData.id);
                  setPopupCur(true);
                }}
              />
              <BiEdit
                fontSize="xx-large"
                className="profile_web_edit"
                onClick={() => {
                  navigate(`/editEstate/${customerEstatesData.id}`);
                }}
              />
            </Flex>

            <Popup trigger={popupCur} setTrigger={setPopupCur}>
              <h3>Are you sure you want to delete this Estate?</h3>
              <br />
              <button
                className="profile_web_edit"
                onClick={() => {
                  DeleteWebsite();
                  setPopupCur(false);
                }}
              >
                Yes
              </button>
            </Popup>
          </Box>
        </Box>
      </div>
    </div>
  );
}

export default MyEstates;
