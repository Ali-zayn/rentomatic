import { Box, Flex, Spacer, Text } from "@chakra-ui/layout";
import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { GoVerified } from "react-icons/go";
import millify from "millify";
import { useParams } from "react-router-dom";

import ImageScrollbar from "./ImageScrollbar";
import { useEffect } from "react";
import axios from "axios";
import { useState } from "react";
import { CgUnavailable } from "react-icons/cg";
import Navbar from "./Navbar";
import React from "react";

const PropertyDetails = () => {
  const [EstateData, setEstateData] = useState(null);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { id } = useParams();
  // Get clicked Website ///////////////////////////////////////////////////////////////////////////////

  const FetchEstate = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `http://127.0.0.1:8000/api/estate/${id}`,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      if (response) {
        setEstateData(response.data);
        setLoading(false);
      }
    } catch (e) {
      setError(e);
      console.error(e.message);
    }
  };
  useEffect(() => {
    FetchEstate();
  }, []);
  if (loading) return "loading...";
  return (
    <>
      <Navbar />
      <Box maxWidth="1000px" margin="auto" p="4">
        {EstateData[0].images && <ImageScrollbar data={EstateData[0].images} />}
        <Box w="full" p="6">
          <Flex
            alignItems="flex-start"
            p="1"
            justifyContent="space-between"
            w="auto"
            color="blue"
          >
            Location : {EstateData[0].city.city_name} ,{EstateData[0].address}{" "}
            {EstateData[0].isAvailable ? (
              <Box color="green">
                <GoVerified /> Furnished
              </Box>
            ) : (
              <Box color="red">
                <CgUnavailable /> Unfurnished
              </Box>
            )}
          </Flex>
          <Flex alignItems="center" p="1" justifyContent="center" w="auto">
            <Text fontWeight="bold" fontSize="lg">
              Contact Number: {EstateData[0].contact_number}
            </Text>
          </Flex>
          <Flex
            alignItems="flex-start"
            p="1"
            justifyContent="space-between"
            w="auto"
            color="black"
          >
            <Text fontWeight="bold" fontSize="lg">
              {EstateData[0].rent_frequency}${EstateData[0].period.rent_period}
            </Text>
            <Text fontSize="lg" marginBottom="2" fontWeight="bold">
              Type : {EstateData[0].type.estate_type}
            </Text>
          </Flex>
          <Spacer />

          <Flex
            alignItems="center"
            p="1"
            justifyContent="center"
            w="auto"
            color="blue"
          >
            <Box display="flex" justifyContent="space-between" w="250px">
              {EstateData[0].rooms}
              <FaBed /> | {EstateData[0].bathrooms} <FaBath /> |{" "}
              {millify(EstateData[0].area)} sqft <BsGridFill />
            </Box>
          </Flex>
        </Box>
        <Box marginTop="2">
          <Text lineHeight="2" color="gray.600">
            {EstateData[0].description}
          </Text>
        </Box>
      </Box>
    </>
  );
};

export default PropertyDetails;
