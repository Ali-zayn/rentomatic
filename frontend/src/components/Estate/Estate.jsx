import { Flex, Box, Text, Button } from "@chakra-ui/react";
//
import Property from "../Property";
import React, { useState, useEffect } from "react";
import Navbar from "../Navbar";
// import { baseUrl, fetchApi } from '../utils/fetchApi';

const Estate = ({ EstatesData, loading, FetchEstates }) => {
  const [searchrterm, setSearchTerm] = useState("");

  const [selectType, setSelectType] = useState(null);
  const [selectRooms, setSelectRooms] = useState(null);
  const [selectBathrooms, setSelectBathrooms] = useState(null);
  const [selectFurnished, setSelectFurnished] = useState(null);
  const [selectRentPeriod, setSelectRentPeriod] = useState(null);
  const [selectRentFrequency, setSelectRentFrequency] = useState(null);
  useEffect(() => {
    FetchEstates();
  }, []);
  console.log(EstatesData);
  return (
    <>
      <Navbar />
      <div className="waviyBody">
        <div className="waviy">
          <span>R</span>
          <span>E</span>
          <span>N</span>
          <span>T&nbsp;</span>
          <span>A&nbsp;</span>
          <span>H</span>
          <span>O</span>
          <span>M</span>
          <span>E</span>
        </div>
      </div>
      <div className="walkingWords">
        <span>At&nbsp;</span>
        <span>Last,&nbsp;</span>
        <span>This&nbsp;</span>
        <span>Is&nbsp;</span>
        <span>What&nbsp;</span>
        <span>You've&nbsp;</span>
        <span>Been&nbsp;</span>
        <span>Searching&nbsp;</span>
        <span>for.&nbsp;</span>
        <span>Welcome&nbsp;</span>
        <span>HOME</span>
      </div>
      <div>
        <form action="" className="search-bar">
          <input
            type="search"
            name="search"
            pattern=".*\S.*"
            onChange={(e) => {
              setSearchTerm(e.target.value);
            }}
          />
          <button className="search-btn" type="submit">
            <span>Search</span>
          </button>
        </form>
        <div className="select-bar">
          <select
            className="trans_select"
            value={selectType}
            onChange={(e) => setSelectType(e.target.value)}
          >
            <option>Type</option>
            <option>Apartment</option>
            <option>Studio</option>
            <option>office</option>
            <option>Villa</option>
          </select>
          <select
            className="trans_select"
            value={selectRentPeriod}
            onChange={(e) => setSelectRentPeriod(e.target.value)}
          >
            <option>Rent Period</option>
            <option>Daily</option>
            <option>Weekly</option>
            <option>Monthly</option>
            <option>Yearly</option>
          </select>
          <select
            className="trans_select"
            value={selectRentFrequency}
            onChange={(e) => setSelectRentFrequency(e.target.value)}
          >
            <option>Rent Frequency</option>
            <option>Below 100$</option>
            <option>Below 500$</option>
            <option>Below 1000$</option>
            <option>Above 1000$</option>
          </select>
          <select
            className="trans_select"
            value={selectRooms}
            onChange={(e) => setSelectRooms(e.target.value)}
          >
            <option>Rooms</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
          <select
            className="trans_select"
            value={selectBathrooms}
            onChange={(e) => setSelectBathrooms(e.target.value)}
          >
            <option>Bathrooms</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
          <select
            className="trans_select"
            value={selectFurnished}
            onChange={(e) => setSelectFurnished(e.target.value)}
          >
            <option>Furnished Status</option>
            <option>Furnished</option>
            <option>Unfurnished</option>
          </select>
        </div>
      </div>
      <Box>
        <Flex flexWrap="wrap">
          {!loading
            ? EstatesData.filter((EstatesData) => {
                if (searchrterm === "") {
                  return EstatesData;
                } else if (
                  EstatesData.city.city_name
                    .toLowerCase()
                    .includes(searchrterm.toLowerCase())
                ) {
                  return EstatesData;
                }
              })
                .filter((EstatesData) => {
                  if (selectType === null) {
                    return EstatesData;
                  } else if (selectType === "Type") {
                    return EstatesData;
                  } else if (
                    EstatesData.type.estate_type
                      .toLowerCase()
                      .includes(selectType.toLowerCase())
                  ) {
                    return EstatesData;
                  }
                })
                .filter((EstatesData) => {
                  if (selectRentPeriod === null) {
                    return EstatesData;
                  } else if (selectRentPeriod === "Rent Period") {
                    return EstatesData;
                  } else if (
                    EstatesData.period.rent_period
                      .toLowerCase()
                      .includes(selectRentPeriod.toLowerCase())
                  ) {
                    return EstatesData;
                  }
                })
                .filter((EstatesData) => {
                  if (selectRentFrequency === null) {
                    return EstatesData;
                  } else if (selectRentFrequency === "Rent Frequency") {
                    return EstatesData;
                  } else if (
                    selectRentFrequency === "Below 100$" &&
                    EstatesData.rent_frequency <= 100
                  ) {
                    return EstatesData;
                  } else if (
                    selectRentFrequency === "Below 500$" &&
                    EstatesData.rent_frequency <= 500
                  ) {
                    return EstatesData;
                  } else if (
                    selectRentFrequency === "Below 1000$" &&
                    EstatesData.rent_frequency <= 1000
                  ) {
                    return EstatesData;
                  } else if (
                    selectRentFrequency === "Above 1000$" &&
                    EstatesData.rent_frequency >= 1000
                  ) {
                    return EstatesData;
                  }
                })
                .filter((EstatesData) => {
                  if (selectRooms === null) {
                    return EstatesData;
                  } else if (selectRooms === "Rooms") {
                    return EstatesData;
                  } else if (EstatesData.rooms == selectRooms) {
                    return EstatesData;
                  }
                })
                .filter((EstatesData) => {
                  if (selectBathrooms === null) {
                    return EstatesData;
                  } else if (selectBathrooms === "Bathrooms") {
                    return EstatesData;
                  } else if (EstatesData.bathrooms == selectBathrooms) {
                    return EstatesData;
                  }
                })
                .filter((EstatesData) => {
                  if (selectFurnished === null) {
                    return EstatesData;
                  } else if (selectFurnished === "Furnished Status") {
                    return EstatesData;
                  } else if (
                    selectFurnished === "Furnished" &&
                    EstatesData.isFurnished === 1
                  ) {
                    return EstatesData;
                  } else if (
                    selectFurnished === "Unfurnished" &&
                    EstatesData.isFurnished === 0
                  ) {
                    return EstatesData;
                  }
                })
                .map((EstatesData) => (
                  <Property EstatesData={EstatesData} key={EstatesData.id} />
                ))
            : "Loading..."}
        </Flex>
      </Box>
    </>
  );
};

export default Estate;
