<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\EstateTypeController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\RentPeriodController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EstateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/login", [CustomerController::class, "Login"]); // login Route
Route::get("/customer/estates/{id}", [
    EstateController::class,
    "customerEstates",
]);
Route::get("/customer/{id}", [CustomerController::class, "show"]);
Route::put("/editCustomer/{id}", [CustomerController::class, "update"]);
Route::post("/addCustomer", [CustomerController::class, "create"]);
Route::get("/estates", [EstateController::class, "estatesAll"]);
Route::get("/estate/{id}", [EstateController::class, "show"]);
Route::put("/editEstates/{id}", [EstateController::class, "update"]);

Route::delete("/deleteEstates/{id}", [EstateController::class, "destroy"]);
Route::post("/addEstate", [EstateController::class, "store"]);
Route::get("/cities", [CityController::class, "index"]);
Route::get("/estateTypes", [EstateTypeController::class, "index"]);
Route::get("/rentPeriods", [RentPeriodController::class, "index"]);

Route::post("/addImages", [ImagesController::class, "store"]);

Route::middleware("auth:sanctum")->get("/user", function (Request $request) {
    return $request->user();
});
