<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class customer extends Model
{
    use HasApiTokens, HasFactory;
    protected $fillable = ["name", "password", "email"];
    public function Estates()
    {
        return $this->hasMany(estate::class, "customer_id");
    }
    public function City()
    {
        return $this->belongsTo(city::class, "city_id");
    }
    public function period()
    {
        return $this->belongsTo(rent_period::class, "rent_period_id");
    }
    public function type()
    {
        return $this->belongsTo(estate_type::class, "estate_type_id");
    }
}
