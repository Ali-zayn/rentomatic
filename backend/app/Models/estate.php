<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class estate extends Model
{
    use HasFactory;
    protected $fillable = [
        "customer_id",
        "city_id",
        "rent_period_id",
        "estate_type_id",
        "isFurnished",
        "cover_image",
        "contact_number",
        "rent_frequency",
        "address",
        "isAvailable",
        "area",
        "rooms",
        "bathrooms",
        "description",
    ];
    public function customer()
    {
        return $this->belongsTo(customer::class, "customer_id");
    }
    public function city()
    {
        return $this->belongsTo(city::class, "city_id");
    }
    public function period()
    {
        return $this->belongsTo(rent_period::class, "rent_period_id");
    }
    public function type()
    {
        return $this->belongsTo(estate_type::class, "estate_type_id");
    }
    public function images()
    {
        return $this->hasMany(images::class);
    }
}
