<?php

namespace App\Http\Controllers;

use App\Models\customer;
use App\Models\estate;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function Login(Request $request)
    {
        $token = Str::random(60);
        $email = $request->input("email");
        $password = $request->input("password");
        $allAdmins = DB::table("customers")
            ->where("password", $password)
            ->where("email", $email)
            ->get();

        return ["status" => $allAdmins, "token" => $token];
    }

    //    public function customerEstates(customer $customer, $id)
    //    {
    //        $customerEstates = customer::where("id", $id)
    //            ->with("Estates")
    //            ->get();
    //
    //        return response()->json([
    //            "customerEstates" => $customerEstates,
    //        ]);
    //    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $customer = customer::create([
                "name" => $request->name,
                "email" => $request->email,
                "password" => $request->password,
            ]);
            $token = $customer->createToken("app")->accessToken;
            return response(
                [
                    "message" => "Registration Successfull",
                    "token" => $token,
                    "customer" => $customer,
                ],
                200
            );
        } catch (Exception $exception) {
            return response(
                [
                    "message" => $exception->getMessage(),
                ],
                400
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new customer();
        $name = $request->input("name");
        $email = $request->input("email");
        $password = $request->input("password");
        $customer->name = $name;
        $customer->email = $email;
        $customer->password = $password;
        $customer->save();
        return response()->json(["customer was created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer, $id)
    {
        $customer = customer::where("id", $id)->get();

        return response()->json([
            "customer" => $customer,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, customer $customer, $id)
    {
        $customer = customer::findOrFail($id);

        $customer->update([
            "name" => $request->name,
            "email" => $request->email,
            "password" => $request->password,
        ]);

        return response([
            "customer" => $customer,
            "message" => "Updated Successfully",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customer)
    {
        //
    }
}
