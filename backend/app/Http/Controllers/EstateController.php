<?php

namespace App\Http\Controllers;

use App\Models\estate;
use App\Models\rent_period;
use Illuminate\Http\Request;

class EstateController extends Controller
{
    public function estatesAll(estate $estate)
    {
        $estateAll = estate::with(
            "customer",
            "city",
            "period",
            "type",
            "images"
        )->get();

        return response()->json([
            "EstatesAll" => $estateAll,
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estate = new estate();
        if ($request->cover_image) {
            $getPicture = $request->cover_image;
            $pictureName = $getPicture->getClientOriginalName();
            $picturePath = public_path() . "/pictures";
            $getPicture->move($picturePath, $pictureName);
            $estate->cover_image = $pictureName;
        }
        $customer_id = $request->input("customer_id");
        $city_id = $request->input("city_id");
        $rent_period = $request->input("rent_period_id");
        $estate_type = $request->input("estate_type_id");
        $isFurnished = $request->input("isFurnished");
        $contact_number = $request->input("contact_number");
        $rent_frequency = $request->input("rent_frequency");
        $address = $request->input("address");
        $isAvailable = $request->input("isAvailable");
        $area = $request->input("area");
        $rooms = $request->input("rooms");
        $bathrooms = $request->input("bathrooms");
        $description = $request->input("description");
        $estate->customer_id = $customer_id;
        $estate->city_id = $city_id;
        $estate->rent_period_id = $rent_period;
        $estate->estate_type_id = $estate_type;
        $estate->isFurnished = $isFurnished;
        $estate->contact_number = $contact_number;
        $estate->rent_frequency = $rent_frequency;
        $estate->rooms = $rooms;
        $estate->isAvailable = $isAvailable;
        $estate->area = $area;
        $estate->address = $address;
        $estate->bathrooms = $bathrooms;
        $estate->description = $description;
        $estate->save();
        return response()->json($estate);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function show(estate $estate, $id)
    {
        $estate = estate::where("id", $id)
            ->with("customer", "city", "period", "type", "images")
            ->get();
        return response()->json($estate);
    }
    public function CustomerEstates(estate $estate, $id)
    {
        $customerEstates = estate::where("customer_id", $id)
            ->with("customer", "city", "period", "type", "images")
            ->get();
        return response()->json([
            "customerEstates" => $customerEstates,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function edit(estate $estate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estate $estate, $id)
    {
        $estate = estate::findOrFail($id);

        if ($request->cover_image) {
            $picturePath = public_path() . "/pictures/";
            //code for remove old file
            if ($estate->cover_image = null && ($estate->cover_image = "")) {
                $file_old = $picturePath . $estate->cover_image;
                unlink($file_old);
            }
            //upload new file
            $file = $request->cover_image;
            $filename = $file->getClientOriginalName();
            $file->move($picturePath, $filename);
            //for update in table
            $estate->cover_image = $filename;
        }

        $inputsestate = $request->except(["_method", "token", "cover_image"]);
        $estate->update($inputsestate);
        return response()->json(["estate updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function destroy(estate $estate, $id)
    {
        $estate = estate::where("id", $id);
        $estate->delete();
        return response()->json(["estate has been deleted"]);
    }
}
