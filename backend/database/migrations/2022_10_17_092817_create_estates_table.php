<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('city_id')
                ->constrained();
            $table->foreignId('rent_period_id')->constrained();
            $table->foreignId('estate_type_id')->constrained();
            $table->boolean("isFurnished")->default(1);
            $table->string('cover_image')->nullable();
            $table->bigInteger("contact_number");
            $table->integer("rent_frequency");
            $table->string('address');
            $table->boolean("isAvailable")->default(1);
            $table->integer("area");
            $table->integer("rooms");
            $table->integer("bathrooms");
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
};
