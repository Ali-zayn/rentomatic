<?php

namespace Database\Seeders;

use App\Models\images;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Images = [
            [
                "estate_id" => 1,
                "image_url" => "Appartment2.jpg",
            ],
            [
                "estate_id" => 1,
                "image_url" => "Appartment2-2.jpg",
            ],
            [
                "estate_id" => 1,
                "image_url" => "Appartment2-3.jpg",
            ],
            [
                "estate_id" => 2,
                "image_url" => "Appartment2-1.jpg",
            ],
            [
                "estate_id" => 2,
                "image_url" => "Appartment2-4.jpg",
            ],
            [
                "estate_id" => 2,
                "image_url" => "Appartment2.jpg",
            ],
            [
                "estate_id" => 3,
                "image_url" => "Achrafieh4-1.jpg",
            ],
            [
                "estate_id" => 3,
                "image_url" => "Achrafieh4-2.jpg",
            ],
            [
                "estate_id" => 3,
                "image_url" => "Achrafieh4-4.jpg",
            ],
            [
                "estate_id" => 4,
                "image_url" => "Achrafieh4-1.jpg",
            ],
            [
                "estate_id" => 4,
                "image_url" => "Achrafieh4-2.jpg",
            ],
            [
                "estate_id" => 4,
                "image_url" => "Achrafieh4-3.jpg",
            ],
            [
                "estate_id" => 5,
                "image_url" => "Appartment2-1.jpg",
            ],
            [
                "estate_id" => 5,
                "image_url" => "Appartment2-2.jpg",
            ],
            [
                "estate_id" => 5,
                "image_url" => "Appartment2-3.jpg",
            ],
            [
                "estate_id" => 6,
                "image_url" => "Appartment2-1.jpg",
            ],
            [
                "estate_id" => 6,
                "image_url" => "Appartment2-4.jpg",
            ],
            [
                "estate_id" => 6,
                "image_url" => "Appartment2.jpg",
            ],
            [
                "estate_id" => 7,
                "image_url" => "Appartment2.jpg",
            ],
            [
                "estate_id" => 7,
                "image_url" => "Appartment2-2.jpg",
            ],
            [
                "estate_id" => 7,
                "image_url" => "Appartment2-3.jpg",
            ],
            [
                "estate_id" => 8,
                "image_url" => "Appartment2-1.jpg",
            ],
            [
                "estate_id" => 8,
                "image_url" => "Appartment2-4.jpg",
            ],
            [
                "estate_id" => 8,
                "image_url" => "Appartment2.jpg",
            ],
            [
                "estate_id" => 9,
                "image_url" => "Achrafieh4-1.jpg",
            ],
            [
                "estate_id" => 9,
                "image_url" => "Achrafieh4-2.jpg",
            ],
            [
                "estate_id" => 9,
                "image_url" => "Achrafieh4-4.jpg",
            ],
            [
                "estate_id" => 10,
                "image_url" => "Achrafieh4-1.jpg",
            ],
            [
                "estate_id" => 10,
                "image_url" => "Achrafieh4-2.jpg",
            ],
            [
                "estate_id" => 10,
                "image_url" => "Achrafieh4-3.jpg",
            ],
        ];
        foreach ($Images as $image) {
            images::create($image);
        }
    }
}
