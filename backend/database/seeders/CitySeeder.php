<?php

namespace Database\Seeders;

use App\Models\city;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Cities = [
            [
                "city_name" => "Beirut"

            ],
            [
                "city_name" => "Sidon"
            ],
            [
                "city_name" => "Byblos"
            ],
            [
                "city_name" => "Tripoli"
            ],
            [
                "city_name" => "Tyre"
            ],
            [
                "city_name" => "Zahle"
            ],
            [
                "city_name" => "Jounieh"
            ],
            [
                "city_name" => "Baalbek"
            ],
            [
                "city_name" => "Baabda"
            ],
        ];
        foreach ($Cities as $city) {
            city::create($city);
        }
    }
}
