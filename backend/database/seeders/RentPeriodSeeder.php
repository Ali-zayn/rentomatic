<?php

namespace Database\Seeders;

use App\Models\rent_period;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RentPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Rent_Periods = [
            [
                "rent_period" => "Yearly"

            ],
            [
                "rent_period" => "Monthly"
            ],
            [
                "rent_period" => "Weekly"
            ],
            [
                "rent_period" => "Daily"
            ],
        ];
        foreach ($Rent_Periods as $rent_Period) {
            rent_period::create($rent_Period);
        }
    }
}
