<?php

namespace Database\Seeders;

use App\Models\estate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EstateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Estates = [
            [
                "customer_id" => 1,
                "city_id" => 1,
                "rent_period_id" => 1,
                "estate_type_id" => 1,
                "isFurnished" => 1,
                "cover_image" => "Beirut-apartment.jpg",
                "contact_number" => "71001002",
                "rent_frequency" => 600,
                "address" => "Hamra",
                "isAvailable" => 1,
                "area" => 200,
                "rooms" => 4,
                "bathrooms" => 3,
                "description" => "RARE Opportunity to rent an apartment on the Downtown of Beirut
                            with beach access and panoramic sea view You can build 7 floors resort mixed between
                             hotel or hotel apartments What the benefits what is the construction cost what is the
                             approximate income for the hotel what is the value of the hotel after completion If you
                             are an investor or International investment company, contact us for full presentation and
                              ready business plan for this opportunity",
            ],
            [
                "customer_id" => 1,
                "city_id" => 1,
                "rent_period_id" => 2,
                "estate_type_id" => 2,
                "isFurnished" => 0,
                "cover_image" => "Appartment2.jpg",
                "contact_number" => "70888899",
                "rent_frequency" => 1000000,
                "address" => "Downtown",
                "isAvailable" => 1,
                "area" => 600,
                "rooms" => 6,
                "bathrooms" => 4,
                "description" => "Sea View Rent To Own Apartment For Rent In Downtown Beirut !

            The Sea View Modern Apartment is located in a brand new branded building in Downtown Beirut, Solidere, Lebanon.

            It is located in the Carre D’or of Mina Al Hosn. Located on a high floor, it possesses open panoramic Sea and Downtown views.

            The Modern flat is composed of 4 bedrooms ( 3 master) and a family room.

            Carre D’or of Mina Al Hosn, Downtown, Beirut is composed of Marina Towers, Karagulla building, Bay Tower, Beirut Tower, Platinum Tower, B11, Beirut Terraces, Damac Versace Beirut, Wadi Hills, Noor Gardens, Dana building and Wadi Grand Residence.

            This Apartment comes with a Rent To Own  Option / Lease To Own Option.

            Beirut Property Link is one of the best Real Estate Agencies in Beirut, Lebanon, with more than 200 active listings in Downtown Beirut.

            Status – Vacant.

            Terms of Payment: External transfer.",
            ],
            [
                "customer_id" => 2,
                "city_id" => 1,
                "rent_period_id" => 3,
                "estate_type_id" => 2,
                "isFurnished" => 1,
                "cover_image" => "Studio1.jpg",
                "contact_number" => "70546897",
                "rent_frequency" => 32,
                "address" => "Solidere",
                "isAvailable" => 1,
                "area" => 51,
                "rooms" => 2,
                "bathrooms" => 1,
                "description" => "The upgraded fully modern sea view studio is located in new development in Beirut, Downtown, Solidere.

            The modern building is at walking distance to main facilities, amenities, restaurants and hotels such as Phoenicia Hotel Beirut and Monroe Hotel Beirut.

            The modern apartment possesses 1 underground parking spot.

            It is composed of 1 kitchen, 1 american kitchen possessing 1 spacious area to accommodate bed, sofa and a balcony with an unobstructed panoramic sea views.

            All ceilings are made of gypsum with modern indirect lighting system.

            All floors including the wet areas are made of fine marble.

            The kitchen is fully fitted out with cabinets and excluding appliances.",
            ],
            [
                "customer_id" => 3,
                "city_id" => 3,
                "rent_period_id" => 4,
                "estate_type_id" => 3,
                "isFurnished" => 0,
                "cover_image" => "Achrafieh4.jpg",
                "contact_number" => "71538928",
                "rent_frequency" => 330,
                "address" => "Achrafieh",
                "isAvailable" => 0,
                "area" => 110,
                "rooms" => 3,
                "bathrooms" => 2,
                "description" => "The apartment for sale in Ashrafieh, Rmeil.

            Its located 5 mins by car from ABC Mall, and walking distance to Roum Hospital.

            110 sqm (fully renovated, building completed in 2000)

            2 Bedrooms

            2 Bathrooms

            3 New split AC units (MDV inverters)
            1 Balcony
            1 Parking underground
            Modern Open plan kitchen (Franke sink, Grohe Mixer, Granite tops and soft close cabinets)

            Water Heater Ariston + Water tank on roof (1000L)

            Floor tiles in all the flat: 120 x 120 Grey concrete Italian ceramics

            Wall tiles in bathroom: 50 x 110 White Italian ceramics

            All WC fixtures are Ideal Standard and Grohe mixers

            All new windows are double glazed, tilt/turn and sliding",
            ],
            [
                "customer_id" => 4,
                "city_id" => 7,
                "rent_period_id" => 4,
                "estate_type_id" => 4,
                "isFurnished" => 1,
                "cover_image" => "Studio1-1.jpg",
                "contact_number" => "71628636",
                "rent_frequency" => 12000,
                "address" => "Nahr El Kalb",
                "isAvailable" => 1,
                "area" => 60,
                "rooms" => 2,
                "bathrooms" => 1,
                "description" => "Sea view fully furnished Chalet for Rent in Holiday Beach. The fully furnished chalet is located in a high floor. It possesses open sea, resort and Marina views. It is composed of 1 bedroom, 1 toilet, 1 open kitchen, 1 living and 1 balcony. It comes with 1 underground parking spot.

            Holiday beach is located in Nahr El Kalb at close destination to main facilities and amenities such as ABC Dbayeh and Le Mall Dbayeh.

            Status – Vacant.

            Terms of Payment: External transfer.",
            ],
            [
                "customer_id" => 1,
                "city_id" => 5,
                "rent_period_id" => 2,
                "estate_type_id" => 1,
                "isFurnished" => 1,
                "cover_image" => "Tripoli1.jpg",
                "contact_number" => "76569342",
                "rent_frequency" => 800,
                "address" => "Al Mina",
                "isAvailable" => 1,
                "area" => 250,
                "rooms" => 5,
                "bathrooms" => 2,
                "description" => "Traditional Lebanese apartment for rent in Tripoli
                            Al Mina consisting of 1 living and 1 dining room opening onto a large balcony.
                            1 fitted kitchen, 3 bedrooms and 2 bathrooms. close to all amenities.",
            ],
            [
                "customer_id" => 4,
                "city_id" => 2,
                "rent_period_id" => 1,
                "estate_type_id" => 2,
                "isFurnished" => 0,
                "cover_image" => "Villa1.jpeg",
                "contact_number" => "70528367",
                "rent_frequency" => 75000,
                "address" => "Al Souk",
                "isAvailable" => 1,
                "area" => 600,
                "rooms" => 6,
                "bathrooms" => 4,
                "description" => "Sea View Rent To Own Apartment  In Sidon !

            The Sea View Modern Apartment is located in a brand new branded building in Al Souk.

            The Villa is composed of 4 bedrooms ( 3 master) and a family room.

            Terms of Payment: External transfer.",
            ],
            [
                "customer_id" => 3,
                "city_id" => 5,
                "rent_period_id" => 4,
                "estate_type_id" => 1,
                "isFurnished" => 1,
                "cover_image" => "Studio1-4.jpg",
                "contact_number" => "70925347",
                "rent_frequency" => 320,
                "address" => "El Hara",
                "isAvailable" => 1,
                "area" => 70,
                "rooms" => 2,
                "bathrooms" => 1,
                "description" => "
            It is composed of 1 kitchen, 1 american kitchen possessing 1 spacious area to accommodate bed, sofa and a balcony with an unobstructed panoramic sea views.

            All ceilings are made of gypsum with modern indirect lighting system.

            All floors including the wet areas are made of fine marble.

            The kitchen is fully fitted out with cabinets and excluding appliances.",
            ],
            [
                "customer_id" => 2,
                "city_id" => 6,
                "rent_period_id" => 3,
                "estate_type_id" => 4,
                "isFurnished" => 0,
                "cover_image" => "Villa1-2.jpeg",
                "contact_number" => "70363528",
                "rent_frequency" => 330,
                "address" => "Wadi El Aaryesh",
                "isAvailable" => 0,
                "area" => 300,
                "rooms" => 3,
                "bathrooms" => 2,
                "description" => "The apartment for sale in ZAhli.

            Its located 5 mins by car from Manara, and walking distance to the park.

            300 sqm (fully renovated, building completed in 2000)

            Modern Open plan kitchen (Franke sink, Grohe Mixer, Granite tops and soft close cabinets)

            Water Heater Ariston + Water tank on roof (1000L)

            Floor tiles in all the flat: 120 x 120 Grey concrete Italian ceramics

            Wall tiles in bathroom: 50 x 110 White Italian ceramics

            All WC fixtures are Ideal Standard and Grohe mixers

            All new windows are double glazed, tilt/turn and sliding",
            ],
            [
                "customer_id" => 2,
                "city_id" => 8,
                "rent_period_id" => 3,
                "estate_type_id" => 1,
                "isFurnished" => 1,
                "cover_image" => "Villa1-2.jpeg",
                "contact_number" => "71628636",
                "rent_frequency" => 30000,
                "address" => "Dores",
                "isAvailable" => 1,
                "area" => 160,
                "rooms" => 4,
                "bathrooms" => 2,
                "description" => "Furnished apartment in the heart of Baalbeck that consists of 1 reception ,
                1 living room ,1 dining room ,kitchen, 3 bedrooms, 2 bathrooms & 1 parking spot.",
            ],
        ];
        foreach ($Estates as $estate) {
            estate::create($estate);
        }
    }
}
