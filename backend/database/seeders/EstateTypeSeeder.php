<?php

namespace Database\Seeders;

use App\Models\estate_type;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EstateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Estate_Types = [
            [
                "estate_type" => "Apartment"

            ],
            [
                "estate_type" => "Studio"
            ],
            [
                "estate_type" => "office"
            ],
            [
                "estate_type" => "Villa"
            ],
        ];
        foreach ($Estate_Types as $estate_Type) {
            estate_type::create($estate_Type);
        }
    }

}
