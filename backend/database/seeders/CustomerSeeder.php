<?php

namespace Database\Seeders;

use App\Models\customer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Customers = [
            [
                "name" => "ELIYA Real Estate",
                "password" => "qwerasdf1234",
                "email" => "eliya_estate@gmail.com",
                "remember_token" => Str::random(10),
            ],
            [
                "name" => "Grad Real Estate",
                "password" => "qwerasdf1234",
                "email" => "grad_estate@gmail.com",
                "remember_token" => Str::random(10),
            ],
            [
                "name" => "We Vision Real Estate",
                "password" => "qwerasdf1234",
                "email" => "we_vision_estate@gmail.com",
                "remember_token" => Str::random(10),
            ],
            [
                "name" => "Spectrum Holding",
                "password" => "qwerasdf1234",
                "email" => "spectrum_holding@gmail.com",
                "remember_token" => Str::random(10),
            ],
        ];
        foreach ($Customers as $customer) {
            customer::create($customer);
        }
    }
}
